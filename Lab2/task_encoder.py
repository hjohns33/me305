""" @file       task_encoder.py
    
    @brief      File responsible for running the encoders.
    @details    When the user wants to interact with the encoders, they will
                be doing so through an object of this class in the main file.
    
    @image      html statetrans0x02-1.jpeg "State-Transition Diagram for Task_Encoder" width=800px
    @author     Holly Johnson
    @date       December 9, 2021
            
    @paragraph  0x02tesc Source Code: 
    @link       https://bitbucket.org/hjohns33/me305/src/master/Lab2/task_encoder.py
"""


import utime
import shares


## @brief   Constant associated with State 0.
S0_INIT = 0
## @brief   Constant associated with State 1.
S1_CLEAR = 1
## @brief   Constant associated with State 2.
S2_COLLECT = 2 
## @brief   Constant associated with State 3.
S3_RECORD = 3


# Define a class for the finite state machine
class Task_Encoder:
    ''' @brief      Update encoder at the requisite interval.
    '''
    
    # Define the constructor
    def __init__(self, f, startTime, enc, enc_pos, delta, dataQ, interrupt, runflag, resetflag):
        ''' @brief      Constructs Task_Encoder object.
            @details    Import parameters relevant to operating encoder.
            @param      f   The frequency the timer is running at. 
            @param      startTime   A specified starting time to know how long the
                        program has been running.
            @param      enc     Object of encoder.Encoder(), capable of accessing
                        the class's functions.
            @param      enc_pos Object of share.Shares() responsible for the encoder
                        position reading.
            @param      delta   Object of share.Shares() responsible for the change
                        in encoder position.
            @param      dataQ   Object of share.Queue() responsible for data transfer.
            @param      interrupt   A flag to tell the program when to interrupt
                        data collection.
            @param      runflag     A flag to tell the program when to start running.
            @param      resetflag   A flag to tell the program when to reset 
                        position.
        '''
        
        ##  @brief  The state of operation the task is in.
        self.state = S0_INIT
        #print('In Task_Encoder State 0: Initialization')
        
        ##  @brief  Period of clock in microseconds converted from frequency in hertz.
        self.period = (1/f)*10^6 #microseconds
        ##  @brief  Time to start counting.
        self.startTime = startTime
        ##  @brief  
        self.next_time = 0
        
        ##  @brief  Encoder being used in this instance of Task_Encoder.
        self.enc = enc
        ##  @brief  Sharable encoder position data.
        self.enc_pos = enc_pos
        ##  @brief  Sharable delta data showing change in position data.
        self.delta = delta

        ##  @brief  A queue for storing data in this instance of Task_Encoder.
        self.dataQ = dataQ
        
        ##  @brief  An interrupt flag for this instance of Task_Encoder.
        self.interrupt = interrupt
        ##  @brief  A reset flag for this instance of Task_Encoder.
        self.resetflag = resetflag
        ##  @brief  A run flag for this instance of Task_Encoder.
        self.runflag = runflag
              
        
    # Define the running operation
    def run(self):
        ''' @brief      Cycle through the operation states to run the encoder task.
            @details    Update states to match state diagram. After initialization,
                        clear all values. Then, cycle between collecting and 
                        recording data until a condition leads to reseting data
                        collection.
        '''
        
        if utime.ticks_us() >= self.next_time:
            # Condition for when time is continuing forward
            
            # Start the time counter
            self.next_time = utime.ticks_us() + self.period
            
            if self.state == S0_INIT:
                # Condition for State 0
                
                # Update to next state
                self.state = S1_CLEAR
        
            if self.state == S1_CLEAR:
                # Condition for State 1
                
                #print('In Task_Encoder State 1: Clear Data')
                
                if  self.resetflag.read():
                    # Wait until told to reset so information isn't lost and 
                    # timer isn't counting needlessly.
                    
                    # Reset the flag
                    self.resetflag.write(0)
                    
                    # Use Encoder.set_position() to set position data to zero.
                    self.enc.set_position(0)
                    
                    # Reset time values
                    self.startTime = 0
                    self.next_time = 0
                    self.resetflag.write(0)
                    
                    
                    # Wait until told to run before collecting and writing data
                    if self.runflag.read():
                        
                        # Update to next state
                        self.state = S2_COLLECT
                        
                        if self.state == S2_COLLECT:
                            # Condition for State 2
                            
                            #print('In Task_Encoder State 2: Collect Data')
                            
                            # Get updated position and delta values
                            self.enc.update()
                            
                            # Update to next state
                            self.state = S3_RECORD
                            
                        if self.state == S3_RECORD:
                            # Condition for State 3
                            
                            #print('In Task_Encoder State 3: Record Data')
                            
                            # Write to shares.Queue object
                            self.enc_pos.write(self.enc.get_position())
                            
                            if self.interrupt.read():
                                # Condition for when interrupt is triggered
                                
                                print('Program has been interrupted by user.')
                                
                                # Reset interrupt flag and send to State 1 for reset.
                                self.interrupt.write(0)
                                self.runflag.write(0)
                                self.state = S1_CLEAR
                            
                            else:
                                # Condition for standard operation, assuming no interrupt
                                
                                ##  @brief  Amount of time that the program has been collecting data for.
                                self.elapsed_time = utime.ticks_diff(utime.ticks_us(), self.startTime.read())
                            
                                if self.elapsed_time >= 30 * 10^6: #microseconds
                                    # Condition for when 30 seconds have passed
                                    
                                    # print('Data has been collected')
                                    
                                    # Function done running and main program awaits further commands
                                    self.runflag.write(0)
                                    
                                    self.state = S1_CLEAR
            
                                    
                                else:
                                    # Condition for when there is time remaining.
                                    
                                    # Write position and delta data to share.Shares objects
                                    self.enc_pos.write(self.enc.get_position())
                                    self.delta.write(self.enc.get_delta())
                                    
                                    # Add all collected data to a tuple for easy packaging
                                    ##  @brief A tuple tying elapsed times and deltas into one package.
                                    self.data_tuple = (self.elapsed_time, self.delta.read())
                                    # Send data to designated data queue
                                    self.dataQ.put(self.data_tuple) 
                                
                                    # Update to next state
                                    self.state = S2_COLLECT
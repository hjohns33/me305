""" @file       touchscreen.py
    @brief      Calibrate and interact with the touch panel.
    @details    Incorporate coupling calibration method to calibrate the touchscreen,
                and create another method to interact with the screen.
    @author     Holly Johnson
    @date       December 10, 2021
"""

import numpy

class Touchscreen:
    
    def __init__(self, xp, xm, yp, ym, W, L):
        ''' @brief      Initialize resistive touch screen panel.
            @details    Set up initial values for operating touch screen.
            @param      xp  Push-pull output asserted high
            @param      xm  Push-pull output asserted low
            @param      yp  Push-pull output asserted high
            @param      ym  Push-pull output asserted low
            @param      W   The width of the panel in the y-axis.
            @param      L   The length of the panel in the x-axis.
        '''
        
        self.xp = xp
        self.xm = xm
        self.yp = yp
        self.ym = ym
        self.W = W
        self.L = L
        
    def calibrate(self, n):
        ''' @brief      Calibrate the resistive touch panel.
            @details    Use ADC ratings, coupling methods, and least squares
                        method to calibrate the touch panel.
            @param      n   Number of calibration samples to use for touchscreen.
        '''
        
        
        # Calibrate x and y at same time to account for coupling.
        # Calibrate using multiple points with least squares method.
        
        # I want to base my calibration by having the user touch the center of
        # the board 5 times. I figure that 3 should be good enough to successfully
        # approximate, and by having them touch the center, which is the point we
        # are trying to balance the ball about, there is less error that would
        # otherwise compound from them touching different corners. That said, 
        # if the touchscreen panel didn't have the grids drawn on then I would
        # have them touch each of the corners 2-3 times and find the center from
        # that information.
        
        # Since the least squares method utilizes much more matrix manipulation, 
        # I'm going to get started with a simple single-sample coupled calibration.
        
        ##  @brief  Coordinates associated with the ADC readings.
        X_tilde = [X
                   Y]
        ##  @brief  Measurements from the ADC.
        X = [ADCx 
             ADCy 
             1]
        
        beta = [Kxx Kxy xo
                Kyx Kyy yo]
        
        
        return 
    
    def run(self):
        ''' @brief      Operate the resistive touch panel.
            @details    Take in information about where the ball is at and 
                        work with control system to balance the ball in the center.
        '''
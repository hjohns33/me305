""" @file       bno055.py
    @brief      The 9-DOF IMU used for reading orientation data.
    @details    The BNO055 will be used with I2C protocol to retrieve
                relevant orientation data. This will also allow us to calibrate
                the system and write in new desired values.
    @author     Holly Johnson
    @date       December 10, 2021
"""

import pyb
from pyb import I2C

class BNO055:
    ''' @brief      Set up the IMU to calibrate and read values.
    '''
    
    def __init__(self, regAddr):
        ''' @brief      Set up constructor for the IMU.
            @param      regAddr     The registration address.
        '''
        
        # Initialize as a master/controller with given clock frequency (Hz)
        self.master = I2C.init(I2C.MASTER, baudrate=400000)
                
        # Initialize as a slave/peripheral with given address
        self.slave = I2C.init(I2C.SLAVE, addr=regAddr)
        
    def opr_mode(self, opr_mode):
        ''' @brief      Change the operating mode of the IMU.
            @param      opr_mode    Operating mode of the IMU.
        '''
        
        #OPR_MODE = 3F (0x3D)
        
        #

    def calib_stat(self):
        ''' @brief      Retrieve the calibration status.
        '''
        
        #CALIB_STAT = 35 (0x35)
        
        #
        I2C.mem_read(data, addr, memaddr, *, timeout=5000, addr_size=8)
        
    def coeffRetrieve(self):
        ''' @brief      Retrieve the calibration coefficients from the IMU as binary data.
        '''
        
        #
        I2C.mem_write(data, addr, memaddr, *, timeout=5000, addr_size=8)
    
    def coeffWrite(self):
        ''' @brief      Write the calibration coefficients from the IMU as binary data.
        '''
        
        #
        I2C.mem_write(data, addr, memaddr, *, timeout=5000, addr_size=8)
    
    def eulerAng(self):
        ''' @brief      Read Euler angles from the IMU.
        '''
        
        # Accelerometer Info:
            # 
        
        #
        I2C.recv(recv, addr=0, *, timeout=5000)
    
    def angVel(self):
        ''' @brief      Read the angular velocity from the IMU.
        '''
        
        # Gyroscope Info:
            # 
        
        #
        I2C.recv(recv, addr=0, *, timeout=5000)
        
    def end(self):
        ''' @brief      Turn off the I2C unit.
        '''
        
        #
        i2c.deinit()



#b'\x01'


## Test Code:
    # import pyb
    # i2c = pyb.I2C(1, pyb.I2C.MASTER)
    # buf = bytearray(1)
    # i2c.mem_read(buf, 0x28, 0x00)
    
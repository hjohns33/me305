''' @file       lab0x02.py

    @brief      This file measures the location of an encoder.
    @details    This file measures how far a motor has spun relative to its
                starting location. It does this by multitasking the encoder
                and user tasks using Shares and Queue objects.
    @image      html task0x02.jpeg "Task Diagram" width=800px
    @author     Holly Johnson   
    @date       December 9, 2021
    
    @paragraph  "Source Code:"
    @link       https://bitbucket.org/hjohns33/me305/src/master/Lab2/lab0x02.py
    
    @paragraph  STM32L476RG Data Sheet: 
    @link       https://www.st.com/resource/en/datasheet/stm32l476je.pdf
'''

# Import relevant external modules.

import pyb
import utime


# Import personal modules.

import encoder
import task_encoder
import task_user
import shares


# Begin main code.

if __name__ == '__main__':  
    
    ##  @brief      Auto-reload value for a 16-bit counter.
    AR = 2^16 - 1
    ##  @brief      Frequency value.
    f = 4 * 10^6 #Hertz
    
    
    
    # Define starting parameters for the first encoder.
    
    ##  @brief      Designate first pin for encoder 1.
    #   @details    Encoder 1 uses pin B6.
    pin1A = pyb.Pin(pyb.Pin.cpu.B6)  
    ##  @brief      Designate second pin for encoder 2.
    #   @details    Encoder 1 uses pin B7.
    pin1B = pyb.Pin(pyb.Pin.cpu.B7) 
    ##  @brief      Designate timer for encoder 1.
    #   @details    Encoder 1 uses timer 4.
    timer1 = pyb.Timer(4, prescaler = 0, frequency=f)
    ##  @brief      The first channel of timer 1.
    #   @details    Channel 1 configures in Encoder mode using the first pin.
    timer1Ch1 = timer1.channel(1, pyb.Timer.ENC_AB, pin=pin1A)
    ##  @brief      The second channel of timer 1.
    #   @details    Channel 2 configures in Encoder mode using the second pin.
    timer1Ch2 = timer1.channel(2, pyb.Timer.ENC_AB, pin=pin1B)
    
    
    # Define starting parameters for the second encoder.
    
    # Note that while the second encoder is set up in this file, we will not 
    # be using it for this lab. It is written to simplify work later.
    
    ##  @brief      Designate first pin for encoder 2.
    #   @details    Encoder 2 uses pin C6.
    pin2A = pyb.Pin(pyb.Pin.cpu.C6) 
    ##  @brief      Designate second pin for encoder 2.
    #   @details    Encoder 2 uses pin C7.
    pin2B = pyb.Pin(pyb.Pin.cpu.C7) 
    ##  @brief      Designate timer for encoder 2.
    #   @details    Encoder 2 uses timer 3.
    timer2 = pyb.Timer(3, prescaler = 0, frequency=f)
    ##  @brief      The first channel of timer 2.
    #   @details    Channel 1 configures in Encoder mode using the first pin.
    timer2Ch1 = timer2.channel(1, pyb.Timer.ENC_AB, pin=pin2A)
    ##  @brief      The second channel of timer 2.
    #   @details    Channel 2 configures in Encoder mode using the second pin.
    timer2Ch2 = timer2.channel(2, pyb.Timer.ENC_AB, pin=pin2B)
    
    
    # Set up share.Shares objects.
    
    ## @brief       Encoder 1's position reading.
    enc1_pos = shares.Share()
    ## @brief       Encoder 2's position reading.
    enc2_pos = shares.Share()
    ## @brief       Encoder 1's delta reading.
    delta1 = shares.Share()
    ## @brief       Encoder 2's delta reading.
    delta2 = shares.Share()
    ## @brief       Reference point to figure out how long encoder 1 has been running.
    startTime1 = shares.Share()
    ## @brief       Reference point to figure out how long encoder 2 has been running.
    startTime2 = shares.Share()
    ## @brief       Flag to tell program whether user wants to retrieve information (instance 1).
    runflag1 = shares.Share()
    ## @brief       Flag to tell program whether user wants to retrieve information (instance 2).
    runflag2 = shares.Share()
    ## @brief       Flag to tell program whether user wants to reset position (instance 1).
    resetflag1 = shares.Share()
    ## @brief       Flag to tell program whether user wants to reset position (instance 2).
    resetflag2 = shares.Share()
    ## @brief       Flag to tell program whether user is interrupting it (instance 1).
    interrupt1 = shares.Share()
    ## @brief       Flag to tell program whether user is interrupting it (instance 2).
    interrupt2 = shares.Share()
    
    
    # Set up share.Queue objects
    
    ## @brief       A queue for data from encoder 1.
    dataQ1 = shares.Queue()
    ## @brief       A queue for data from encoder 2.
    dataQ2 = shares.Queue()
    ## @brief       A queue for the current time of the system.
    #  @details     Storing the time in a queue guarantees that time is not being
    #               frozen while program is waiting for user input.
    currentTime = shares.Queue()
    
    
    # Set up objects of classes.
    
    ##  @brief      Create an object of class Encoder for encoder 1.
    enc1 = encoder.Encoder(pin1A, pin1B, AR, timer1, timer1Ch1, timer1Ch2)
    ##  @brief      Create an object of class Encoder for encoder 2.
    enc2 = encoder.Encoder(pin2A, pin2B, AR, timer2, timer2Ch1, timer2Ch2)
    
    
    
    # Set up tasks for the system.
    
    ##  @brief      Create an object of class Task_Encoder for encoder 1.
    enc1T = task_encoder.Task_Encoder(f, startTime1, enc1, enc1_pos, delta1, dataQ1, interrupt1, runflag1, resetflag1)
    ##  @brief      Create an object of class Task_Encoder for encoder 2.
    enc2T = task_encoder.Task_Encoder(f, startTime2, enc2, enc2_pos, delta2, dataQ2, interrupt2, runflag2, resetflag2)
    ##  @brief      Set up Task_User to access user interface.
    userT = task_user.Task_User(enc1_pos, delta1, interrupt1, runflag1, resetflag1)

    
    while 1:
        
        try:
            
            # Run encoder 1.
            enc1T.run()
            # Run encoder 2.
            enc2T.run()
            # Run user interface.
            userT.key_press()
            
            # All interactions between these tasks is dictated by the user.
            
        except KeyboardInterrupt:
            
            break
        
    print('Program Terminating')
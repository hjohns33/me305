""" @file       encoder.py
    
    @brief      Establish basic encoder functions.
    @details    This file operates as the encoder driver. Its operations are run
                through a task file.
    @author     Holly Johnson
    @date       December 9, 2021
                
    @paragraph  0x02sc Source Code: 
    @link       https://bitbucket.org/hjohns33/me305/src/master/Lab2/encoder.py
"""


class Encoder:
    ''' @brief          Interface with quadrature encoders.
    '''

    # Define the constructor
    def __init__(self, pinA, pinB, AR, timer, timerCh1, timerCh2):
        ''' @brief      Constructs an encoder object.
            @details    Dedicates pins and timers used in a STM32L476RG 
                        microcontroller, and sets up local variables for
                        future use within class Encoder.
            @param      pinA    The pin designation for the input.
            @param      pinB    The pin designation for the output.
            @param      AR      The AR value provided by the manufacturer. In
                        this case, the max. value is 16-bits.
            @param      timer   The timer designation.
            @param      timerCh1    The timer's first channel designation.
            @param      timerCh2    The timer's second channel designation.
        '''
        
        # Set up Pins and Timers for this object of the class
        
        ##  @brief  The input pin.
        self.pin1 = pinA
        ##  @brief  The output pin.
        self.pin2 = pinB
        ##  @brief  Number the timer can count up to (referred to as 'period' in lab manual).
        self.period = AR
        ##  @brief  The timer.
        self.tim = timer
        ##  @brief  The timer's first channel.
        self.timCh1 = timerCh1
        ##  @brief  The timer's second channel.
        self.timCh2 = timerCh2

        
        # Set all future variables used to 0
        ##  @brief  The current position read by the encoder.
        self.currentPos = 0
        ##  @brief  The change in position between encoder positions.
        self.delta = 0
        
    def update(self):
        ''' @brief      Updates encoder position and delta.
            @details    Find delta reading using 16-bit counter, then
                        check whether delta falls within underflow or overflow
                        conditions. From there, add delta to the position value.
        '''
        
        # Find where the current position lies on the AR chart
        ##  @brief  The current position relative to an auto reload chart.
        self.oldPos = self.currentPos % self.period
        
        # Retrieve the current 16-bit AR reading
        ##  @brief  The raw counter reading.
        self.newPos = self.tim.counter()
        
        # Find the "actual" delta, the difference between the two positions
        self.delta = self.newPos - self.oldPos
        
        
        # Case for underflow
        if self.delta <= - self.period / 2:
            self.delta += self.period
            
        # Case for overflow
        if self.delta >= self.period / 2:
            self.delta -= self.period
        
        # Increase position by "true" delta value
        self.currentPos += self.delta
        
    def get_position(self):
        ''' @brief      Retrieves the current encoder position.
            @return     self.currentPos The current position of the encoder shaft.
        '''
        
        # Position was found in void function, set_position().
        
        return  self.currentPos
    
    def set_position(self, position):
        ''' @brief      Sets encoder position to the current position.
            @details    While this can be used to label the position value as
                        anything, it will be used predominantly to reset the
                        position value.
            @param      position The new position of the encoder shaft.
        '''
        
        # Reset the timer counter.
        self.tim.counter(0)
        # Update the variable for the encoder position to be the current reading.
        self.currentPos = position

    
    def get_delta(self):
        ''' @brief      Returns encoder delta value.
            @details    Delta is calculated in update(), and it tells the user
                        how much the encoder shaft has rotated.
            @return     self.delta  The change in position of the encoder shaft
                        between the two most recent updates.
        '''
       
        return self.delta
    
''' @file                homeworkpage.py
    @author              Holly Johnson
    @date                December 9, 2021

    @brief               Homework 0x02 and 0x03 pages.
    @details             This page contains all information from homeworks
                         0x02 and 0x03.

    @page                Homework
   
    @section hw_intro    Introduction
                         This page contains all information from homeworks
                         0x02 and 0x03. Doxygen is currently experiencing issues
                         in the image and file paths to incorporate the
                         assignments, so they will be included as google drive
                         links temporarily

    @section hw0x02      Homework 0x02
                         The following hand calculations show the setup for
                         finding the system matrix in second order form.
    @image               html hw0x02p01.png width=800px
    @image               html hw0x02p02.png width=800px
    @image               html hw0x02p03.png width=800px
    @image               html hw0x02p04.png width=800px
    @image               html hw0x02p05.png width=800px
    @image               html hw0x02p06.png width=800px
    @image               html hw0x02p07.png width=800px
    @image               html hw0x02p08.png width=800px
    @image               html hw0x02p09.png width=800px
    @image               html hw0x02p10.png width=800px
    @image               html hw0x02p11.png width=800px
   
    @section hw0x03      Homework 0x03
    @image               html hw0x03p01.png width=800px
    @image               html hw0x03p02.png width=800px
    @image               html hw0x03p03.png width=800px
    @image               html hw0x03p04.png width=800px
    @image               html hw0x03p05.png width=800px
    @image               html hw0x03p06.png width=800px
'''


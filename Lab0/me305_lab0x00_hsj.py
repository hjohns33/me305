# -*- coding: utf-8 -*-
"""@file me305_lab0x00_hsj.py
Created on Tue Sep 21 12:31:31 2021
@author: hollyjohnson
"""

def fib (idx):  #define function "fib" 
    #this section contains code defining function fib in terms of input idx
    f0 = 1  #define value of index 0
    f1 = 1  #define value of index 1
    if idx == '0':
        # condition for when index = 0
        fn = f0 #fibonacci number at index 0 is already defined
        return fn
    try:
        #if input doesn't work in this block, it moves on to the except term
        idx = int(idx)  #index is stored as integer
        if idx > 1:
            #condition for if index is greater than 1
            x,y,n = f0,f1,2  
                #assign f0 to variable x and f1 to variable y for readability
                #n stores the index position while program is running
            while n < idx:
                #loops the process until index n is equal to the index input
                x,y,n = y,x+y,n+1
                    #redefine x, y, and n for each step of process
                    #this method avoids storing unnecessary data and buffering
            fn = x+y    #define fibonacci number as sum of previous two indeces
            return fn
        else:
            if idx == 1:
                #condition for when index = 1
                fn = f1 #fibonacci number at index 1 is already defined
                return fn
            elif idx < 0:
                #condition for when index is negative
                #could be else instead of an elif
                print('The index must be non-negative.')
    except:
        #condition for when index is not a whole number
        print('The index must be a whole number.')
    return 0

if __name__ == '__main__':
    #this section contains the user interface code
    while 1:
        #run program as a loop until break condition satisfied
        idx = input('Please enter an index number: ') #user defines index input
        fibnum = fib(idx)    #run program and store numerical output as fibnum
        if fibnum == 0:
            #prevents program from displaying program outputs
            print('Please try again.')
        else:
            #display program outputs
            print ('Fibonacci number at '
                   'index {:} is {:}.'.format(idx,fibnum))
        a = input('Enter q to quit or press enter to continue: ')   
            #allow user the option to quit program or keep program running
        if a == 'q':
            #quit program when q selected
            print(' --- end program --- ')
            break
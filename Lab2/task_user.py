""" @file       task_user.py

    @brief      Set up user interface with board.
    @details    This class implements all user commands. Commands are 
                set up as single-character shortcuts. 'z' zeros the position
                of encoder 1, 'p' prints the position of encoder 1, 'd' prints
                the delta value for encoder 1, 'g' collects and prints encoder
                1 data for 30s, and 's' ends data collection prematurely.
                Ctrl + C is the keyboard interrupt which ends the entire program.
    @image      html statetrans0x02-2.jpeg "State-Transition Diagram for Task_User" width=800px
    @author     Holly Johnson
    @date       December 9, 2021
    
    @paragraph  0x02tusc Source Code: 
    @link       https://bitbucket.org/hjohns33/me305/src/master/Lab2/task_user.py
"""

# Import Relevant Modules
import array
import USB_VCP


class Task_User:
    
    def __init__(self, enc1_pos, delta1, interrupt1, runflag1, resetflag1):
        ''' @brief      The constructor for class Task_User
            @details    Initialize and wait for user input
            @param      
        '''
        
        ##  @brief  Sharable encoder position data for encoder 1.
        self.enc1_pos = enc1_pos
        ##  @brief  Sharable delta data showing change in position data for encoder 1.
        self.delta1 = delta1
        ##  @brief  An interrupt flag for this instance of Task_User.
        self.interrupt1 = interrupt1
        ##  @brief  A run flag for this instance of Task_User.
        self.runflag1 = runflag1
        ##  @brief  A reset flag for this instance of Task_User.
        self.resetflag1 = resetflag1
        
        # Build array to store future data collection values
        ##  @brief  An array for storing collected data during 'g' command.
        self.data_array = array.array('d', [0])
        
        print('Task_User is constructed. User commands are as follows:'
              'z: Zero the position of encoder 1. '
              'p: Print out the position of encoder 1. '
              'd: Print out the delta for encoder 1. '
              'g: Collect encoder 1 data for 30s and print to PuTTY. '
              's: End data collection prematurely.'
              'Ctrl + C: End program.')
    
    def key_press(self):
        ''' @brief      This function carries out user input commands.
            @details    When a character is input, read the character and
                        carry out the user command correlated with it.
            @param      encoderT    Instance of task encoder
        '''
        
        if USB_VCP.any:
            
            ##  @brief  The command key that has been pressed by the user.
            self.myChar = USB_VCP.read
            
            # Define 'z' input
            if self.myChar == b'z':
                
                self.resetflag1.write(1)
                
                print('Encoder 1 position set to zero.')
            
            # Define 'p' input
            elif self.myChar == b'p':
                
                print('Encoder 1 position is: ', self.enc_pos.read(1))
            
            # Define 'd' input
            elif self.myChar == b'd':
                
                print('Encoder 1 delta is: ', self.delta.read(1))
                
            # Define 'g' input
            elif self.myChar == b'g':
                
                self.runflag1.write(1)
#                # Reset positon value
#                self.enc.reset()
#                # Run program for 30s
#                startTime = utime.ticks_ms()
#                while 1:
#                    encoderT.run()
#                    # record a value to an array
#                    if my_Q.num_in() > 0:
#                        print(my_Q.get())
#                    stopTime = utime.ticks_ms()
#                    duration = utime.ticks_diff(stopTime, startTime)
#                    if duration >= 30:
#                        break
#                #enc_pos.read()
#                #dataQ.get()
#                #self.data_array = 
                print('Position data collected for Encoder 1: ',
                       self.data_array)
                
            # Define 's' input
            elif self.myChar == b's':
                
                print('Data collection ending prematurely...')
                
                # Set data interrupt flag to true
                self.interrupt1.write(1)
                
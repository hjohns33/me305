""" @file       drv8847.py
    @author     Holly Johnson
    @date       December 9, 2021
    
    @brief      Set up motor driver and motor objects correlating to that driver.
    @details    This file contains all of the functions needed for running motors,
                and the process these functions will be used in will be laid out 
                in task_motor.py.
"""

# similar structure to tim.channel use, creating motor objects

import pyb

class DRV8847:
    ''' @brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or more objects of the
                    Motor class which can be used to perform motor
                    control. 
        @paragraph  DRV8847 Spec Sheet: 
        @link       https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self, timer, nSLEEP, nFAULT, faultFlag):
        ''' @brief  Initializes and returns a DRV8847 object.
        '''
        
        self.tim = timer
        self.nSLEEP = nSLEEP
        self.nFAULT = nFAULT
        self.faultFlag = faultFlag
    
    def enable (self):
        ''' @brief  Brings the DRV8847 out of sleep mode.
        '''
        
        # Disable faults, enable motor driver
        self.nSLEEP.high()
        
        # Wait to enable so fault condition not accidentally triggered (100ms)
        pyb.delay(100)
        
        # Enable faults again
        
        
    
    def disable (self):
        ''' @brief  Puts the DRV8847 in sleep mode.
        '''
        
        self.nSLEEP.low()
    
    def fault_cb (self, IRQ_src):
        ''' @brief  Callback function to run on fault condition.
            @param  IRQ_src The source of the interrupt request.
        '''
        
        #ignore callbacks for 25ms, and resume callback
        #pyb.fault_debug(False)
        
        self.disable()
        self.faultFlag.write(1)

    
    def motor (self, motor):
        ''' @brief  Initializes and returns a motor object associated with the DRV8847.
            @return An object of class Motor
        '''
        
        if motor == 1:
            # Condition for calling the first motor.
            
            return Motor(self.tim, 1, pyb.Pin(pyb.Pin.cpu.B4), 2, pyb.Pin(pyb.Pin.cpu.B5))
            #incorporate vars into main code? or class motor? or self.var format?
        
        if motor == 2:
            # Condition for calling the first motor.
            
            return Motor(self.tim, 3, pyb.Pin(pyb.Pin.cpu.B0), 4, pyb.Pin(pyb.Pin.cpu.B1))
            #incorporate vars into main code? or class motor? or self.var format?

    
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
        @paragraph  Maxon DCS22S Motor Spec Sheet:
        @image      html    dcx22s_p01.png  width=800px
        @image      html    dcx22s_p02.png  width=800px
        @image      html    dcx22s_p03.png  width=800px
        @image      html    dcx22s_p04.png  width=800px
        @image      html    dcx22s_p05.png  width=800px
    '''

    def __init__ (self, timer, timerCh1, Ch1Pin, timerCh2, Ch2Pin):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
        '''
        
        self.ch1 = timer.channel(timerCh1, pyb.Timer.PWM, pin=pinA)
        self.ch2 = timer.channel(timerCh2, pyb.Timer.PWM, pin=pinB)
        #doesn't currently correlate with main code and encoder files, update
        
        pass
    
    def set_duty (self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                        cycle of the PWM signal sent to the motor.
        '''
        
        if duty < -100:
            # Condition for duty below lower limit
            
            print('Duty invalid, duty must have a magnitude below 100%.')
            #have this auto set duty to -100? probably a risky move :/
            #could have automatically set duty to 0 instead?
            
        elif duty < 0:
            # Condition for positive duty, CCW spinning
            
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(-duty) #Want positive input to CCW pin
            
        elif duty > 0:
            # Condition for positive duty, CW spinning
            
            self.ch1.pulse_width_percent(duty) #Want positive input to CW pin
            self.ch2.pulse_width_percent(0)
            
        elif duty > 100:
            # Condition for duty above upper limit
            
            print('Duty invalid, duty must have a magnitude below 100%.')
            #same note as for duty<-100
            
        else:
            # Condition for duty of zero or completely invalid duty.
            
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(0)
            #possible issue: entering non-numeric characters may cause duty to
            #go to zero
            #keeping in here for now because it's better than value being 
            #completely ignored and user not getting any feedback
        
        # self.t4ch1.pulse_width_percent(100*((duty/1000) % 1.0 < 0.5)) 
    
    
    
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.


    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
    
    motor_drv = DRV8847()
    motor_1 = motor_drv.motor()
    motor_2 = motor_drv.motor()
    
    # Enable the motor driver
    motor_drv.enable()
    
    # Set the duty cycle of the first motor to 10 percent and the duty cycle of
    # the second motor to 20 percent
    motor_1.set_duty(10)
    motor_2.set_duty(20)
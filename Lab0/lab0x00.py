""" @file       lab0x00.py
    
    @brief      This file calculates the Fibonacci number for an input.
    @details    The user is asked to provide an input value to find the 
                Fibonacci number of. If the input is valid, then the program
                will calculate the output. If it isn't, the program will 
                provide feedback on what made the value invalid. After each
                input, the user is given the option to enter another index 
                number or to quit the program.
    @author     Holly Johnson
    @date       September 21, 2021
                
    @paragraph  0x00p1  Source Code: 
    @link       https://bitbucket.org/hjohns33/me305/src/master/Lab0/lab0x00.py
"""


def fib (idx):
    ''' @brief      This function handles calculations of the sequence
        @details    This section contains code defining the function, fib(), 
                    in terms of the input, idx. 
        @param      idx     The input index number from the user.
        @return     fn      The Fibonacci number at the index.
    ''' 
    
    ##  @brief      Define the value of index 0.
    #   @details    The Fibonacci number for index 0 is 1.
    f0 = 1  
    ##  @brief      Define the value of index 1.
    #   @details    The Fibonacci number for index 1 is 1.
    f1 = 1  
    
    if idx == '0':
        # Condition for when the index is 0.
        
        ##  @brief      Select the Fibonacci number for index 0.
        #   @details    Since the Fibonacci number for index 0 is already 
        #               defined and requires no math, simply set the return
        #               value equal to the pre-defined value.
        fn = f0 
        # Return the Fibonacci number.
        return fn
    
    try:
        # If input doesn't work in this block, it moves on to the except term.
        
        # Make sure that idx is stored as an integer for future
        idx = int(idx)  
        
        if idx > 1:
            # Condition for if index is greater than 1.
            
            ##  @brief      Set variables to use in future equations.
            #   @details    Assign f0 to variable x and f1 to variable y. 
            #               Assign n to index position. The first index
            #               position to require processing is index 2.
            x,y,n = f0,f1,2  

            
            while n < idx:
                # Loop the process until index n is equal to the index input.
                
                # Re-define x, y, and n to increase by one index for each step.
                # This method avoids storing unnecessary data which would 
                # cause the program to buffer at higher values.
                x,y,n = y,x+y,n+1
                
            # Define the Fibonacci number as a sum of the previous 2 indices.
            fn = x+y
            # Return the Fibonacci number.
            return fn
        
        else:
            # Condition for when the index is not greater than 1.
            
            if idx == 1:
                # Condition for when the index is 1.
                
                ##  @brief      Select the Fibonacci number for index 1.
                #   @details    Since the Fibonacci number for index 1 is 
                #               already defined and requires no math, simply 
                #               set the return value equal to the pre-defined 
                #               value.
                fn = f1 
                # Return the Fibonacci number.
                return fn
            
            elif idx < 0:
                # Condition for when index is negative.
                # an "else" statement would also work here.
                
                # Print feedback for the user to know why the input is invalid.
                print('The index must be non-negative.')
                
    except:
        # Condition for when index is not a whole number.
        
        # Print feedback for the user to know why the input is invalid.
        print('The index must be a whole number.')
        
    # Return nothing for invalid index inputs.
    return 0


if __name__ == '__main__':
    # The user interface code is stored here. Keeping the function outside
    # of this section allows for the main code to be more readable. 
    # This if statement makes it so that the program only runs when told to,
    # and not when the program is simply imported.
    
    while 1:
        # Run the program as a loop until the break condition is satisfied.
        
        ##  @brief      User defines index number.
        #   @details    Program asks user to provide an index number stored
        #               as variable idx. The index number is the index of the 
        #               Fibonacci sequence, starting from zero.
        idx = input('Please enter an index number: ')
        
        ##  @brief      Run the program and store the numerical output.
        #   @details    Run the function fib(idx) and store the numerical
        #               output as variable fibnum.
        fibnum = fib(idx)    
        
        if fibnum == 0:
            # Condition for when fib(idx) returns 0.
            # This means the input was invalid.

            print('Please try again.')
            
        else:
            # Condition for when a value for the Fibonacci number is returned.
            
            # Display program input and correlating output.
            print ('Fibonacci number at '
                   'index {:} is {:}.'.format(idx,fibnum))
            
        ##  @brief      Temporary variable for next user input
        #   @details    Ask the user whether they would like to quit the
        #               program or keep the program running.
        a = input('Enter q to quit or press enter to continue: ')   
            
        if a == 'q':
            # Condition for when user inputs 'q'.
            # If this condition is not met, the loop will continue to ask for 
            # inputs and return outputs.

            # Let user know program is ending.
            print(' --- end program --- ')
            # End program.
            break
        
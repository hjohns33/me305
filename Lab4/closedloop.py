""" @file       closedloop.py
    @author     Holly Johnson
    @date       December 9, 2021
    
    @brief      Provide closed loop control for the system.
    @details    This file sets up gain for a controller, which will be used for
                improved velocity responses.
                
    
"""


class ClosedLoop:
    ''' @brief      Set up control gain and interact with it as desired.
    '''
    
    def __init__(self, Lmin, Lmax):
        ''' @brief      Constructor for closed-loop controller.
            @details    
            @param      Lmin    Minimum acceptable duty cycle.
            @param      Lmax    Maximum acceptable duty cycle.
        '''
        self.Lmin = Lmin
        self.Lmax = Lmax
        
    def run(self, omega_ref, omega, Kp):
        ''' @brief      Compute and return the actuation value.
            @details    Calculate PWM level and make sure it's within physical
                        limits.
            @param      omega_ref   The reference/desired angular velocity.
            @param      omega       The actual velocity of the output shaft.
            @param      Kp          The P control gain value.
            @return     self.L      The PWM level
        '''
        
        ## @brief   The PWM level.
        #  @details L is the ratio of voltage applied to the motor, V_m, to 
        #           voltage V_DC.
        self.L = Kp * (omega_ref - omega)
        
        # Make sure PWM level doesn't exceed maximum. If so, set to max value.
        if self.L > self.Lmax:
            self.L = self.Lmax
            
        # Make sure PWM level doesn't exceed minimum. If so, set to min value.
        if self.L < self.Lmin:
            self.L = self.Lmin
        
        # Return PWM level
        return self.L
        
    def get_Kp(self):
        ''' @brief      Find the Kp value.
            @return     self.Kp  The gain value of the closed loop in [%/RPM]
        '''
        
        # Return the gain value
        return self.Kp
        
    def set_Kp(self, Kp):
        ''' @brief      Modify the gain value to represent the present value.
            @param      Kp  The P control gain value.
        '''
        
        self.Kp = Kp